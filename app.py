def convert_to_binary(number):
    return bin(number)[2:]


def main():
    number = input("Enter number: ")
    print("Binary result : " + convert_to_binary(int(number)))


if __name__ == "__main__":
    main()
